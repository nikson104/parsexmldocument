package by.parsers;


import java.util.Date;

public class Patient {
    private int id;
    private String name;
    private String surname;
    private Date dateBirthday;
    private boolean illness;

    Patient() {
    }

    public Patient(int id, String name, String surname, Date dateBirthday, boolean illness) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.dateBirthday = dateBirthday;
        this.illness = illness;
    }


    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append("Name ").append(name).append(";");
        out.append(" Surname ").append(surname).append(";");
        out.append(" Date of Birthday ").append(dateBirthday).append(";");
        out.append(" illness ").append(illness).append(";").append("\n");
        return out.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDateBirthday() {
        return dateBirthday;
    }

    public void setDateBirthday(Date dateBirthday) {
        this.dateBirthday = dateBirthday;
    }

    public boolean isIllness() {
        return illness;
    }

    public void setIllness(boolean illness) {
        this.illness = illness;
    }
}
