package by.parsers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DomParser {
    private final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private Document document;

    public static Element fromRootElement() throws ParserConfigurationException, IOException, org.xml.sax.SAXException {
        File inputFile = new File("patients.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        Document document = documentBuilder.parse(inputFile);
        return document.getDocumentElement();
    }

    private static Date parseDate(String date) throws ParseException {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.parse(date);
    }

    public static List<Patient> patient(Element root) throws ParseException {
        List<Patient> list = new ArrayList<>();
        for (int i = 0; i < root.getChildNodes().getLength(); i++) {
            Node patientNode = root.getChildNodes().item(i);
            if (patientNode.getNodeType() == Node.ELEMENT_NODE) {
                Element patientElement = (Element) patientNode;
                String firstnamee = patientElement.getElementsByTagName("firstname").item(0).getTextContent();
                String lastname = patientElement.getElementsByTagName("lastname").item(0).getTextContent();
                Date dateOfBithday = parseDate(patientElement.getElementsByTagName("dateOfBithday").item(0).getTextContent());
                boolean illness = Boolean.valueOf(patientElement.getElementsByTagName("illness").item(0).getTextContent());
                int id = Integer.parseInt(patientElement.getAttribute("id"));
                list.add(new Patient(id, firstnamee, lastname, dateOfBithday, illness));
            }
        }
        return list;
    }
}
