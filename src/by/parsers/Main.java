package by.parsers;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;

public class Main {
    public static void main(String[] args) {
        SaxParser saxParser = new SaxParser();
        try {
            System.out.println(DomParser.patient(DomParser.fromRootElement()));
            saxParser.parse();
        } catch (ParserConfigurationException | SAXException | IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}