package by.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SaxParser extends DefaultHandler {

    boolean nameB = false;
    boolean surnameB = false;
    boolean dateBirthdayB = false;
    boolean illnessB = false;
    private Patient patient;
    private List<Patient> patients = new ArrayList<>();


    public SAXParser getSAXParser() throws ParserConfigurationException, SAXException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        return factory.newSAXParser();
    }

    public void parse() throws ParserConfigurationException, SAXException, IOException {
        SAXParser parser = getSAXParser();
        File inputFile = new File("patients.xml");
        parser.parse(inputFile, this);
        System.out.println("Patients from SAX:" + patients);
    }

    @Override
    public void startElement(String uri,
                             String localName, String qName, Attributes attributes) throws SAXException {

        if (qName.equalsIgnoreCase("patient")) {
            patient = new Patient();
            patient.setId(Integer.valueOf(attributes.getValue("id")));
        } else if (qName.equalsIgnoreCase("firstname")) {
            nameB = true;
        } else if (qName.equalsIgnoreCase("lastname")) {
            surnameB = true;
        } else if (qName.equalsIgnoreCase("dateOfBithday")) {
            dateBirthdayB = true;
        } else if (qName.equalsIgnoreCase("illness")) {
            illnessB = true;
        }
    }

    @Override
    public void endElement(String uri,
                           String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("patient")) {
            patients.add(patient);
            patient = null;
        }
    }

    private Date parseDate(String date) throws ParseException {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.parse(date);
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {

        if (nameB) {
            patient.setName(new String(ch, start, length));
            nameB = false;
        } else if (surnameB) {
            patient.setSurname(new String(ch, start, length));
            surnameB = false;
        } else if (illnessB) {
            patient.setIllness(Boolean.valueOf(new String(ch, start, length)));
            illnessB = false;
        } else if (dateBirthdayB) {
            try {
                patient.setDateBirthday(parseDate(new String(ch, start, length)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dateBirthdayB = false;
        }
    }
}